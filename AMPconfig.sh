#  Copyright 2019 Owais Shaikh
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/bin/bash

cd ~
sudo
clear;
echo -e "**********************"
echo -e "*     AMP config     *"
echo -e "**********************"

echo -e "Press '1' to START all AMP services";
echo -e "Press '0' to STOP all AMP services";

echo -e "___";

echo -e "Press 'e' to enable AMP services on boot";
echo -e "Press 'd' to disable AMP services on boot";
echo -e "Press 'o' for more options";

echo -e "___\n";

read choice;

if [ $choice == 1 ]
then
   sudo systemctl restart httpd
   sudo systemctl restart mysqld
   echo -e "---\nYour local IP is: "
   ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'
   echo -e "Your public IP is:";
   curl ifconfig.me
   echo -e "\nStarted all services! :)";
elif [ $choice == 0 ]
then
   sudo systemctl stop httpd
   sudo systemctl stop mysqld
   echo -e "---\nStopped all services!";
elif [ $choice == e ]
then
   sudo systemctl enable httpd
   sudo systemctl enable mysqld
   echo -e "---\nEnabled on boot";
elif [ $choice == d ]
then
      sudo systemctl disable httpd
      sudo systemctl disable mysqld
      echo -e "---\nDisabled on boot";

elif [ $choice == o ]
then
   echo -e "Press 's' to configure everything with default settings";
   echo -e "Press 'a' to install AMP (ARCH LINUX ONLY)";
   echo -e "Press 'm' to install AMP (MAC OS X ONLY)";
   echo -e "Press 'i' to create new MariaDB password";
   echo -e "---\n"
   read altchoice;
   
   if [ $altchoice == i ]
   then
      sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
      sudo mysql_secure_installation
   
   elif [ $altchoice == a ]
   then
      sudo pacman -S apache
      sudo pacman -S mysql
      sudo pacman -S php php-apache
      sudo pacman -S phpmyadmin

   elif [ $altchoice == m ]
   then
      brew install apache
      brew install mysql
      brew install php
      brew install php-apache
      brew install phpmyadmin

   elif [ $altchoice == s ]
   then
      sudo echo "LoadModule unique_id_module modules/mod_unique_id.so" | tee -a /etc/httpd/conf/httpd.conf
      sudo systemctl restart httpd
      sudo chmod 755 /srv/http/
      sudo echo "<html><body><h1>Apache server running!</h1><br><p>Files located at /srv/http</p></body></html>" | tee -a /srv/http/index.html
      sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
      sudo systemctl start mysqld
      sudo mysql_secure_installation
      sudo echo "LoadModule mpm_event_module modules/mod_mpm_event.so" | tee -a /etc/httpd/conf/httpd.conf
      sudo echo "LoadModule mpm_prefork_module modules/mod_mpm_prefork.so" | tee -a /etc/httpd/conf/httpd.conf
      sudo echo "LoadModule php7_module modules/libphp7.so" | tee -a /etc/httpd/conf/httpd.conf
      sudo echo "AddHandler php7-script php" | tee -a /etc/httpd/conf/httpd.conf
      sudo echo "Include conf/extra/php7_module.conf" | tee -a /etc/httpd/conf/httpd.conf
      sudo echo "<?php phpinfo(); ?>" /srv/http/start.php
      sudo systemctl restart httpd
      sudo echo "extension=bz2" | tee -a /etc/php/php.ini
      sudo echo "extension=mysqli" | tee -a /etc/php/php.ini
      sudo echo "Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "<Directory "/usr/share/webapps/phpMyAdmin">" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "DirectoryIndex index.php" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "AllowOverride All" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "Options FollowSymlinks" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "Require all granted" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "</Directory>" | tee -a /etc/httpd/conf/extra/phpmyadmin.conf
      sudo echo "Include conf/extra/phpmyadmin.conf" | tee -a /etc/httpd/conf/httpd.conf
      sudo systemctl restart httpd

      echo -e "Done! open your browser and go to localhost/start.php";
   fi
fi
