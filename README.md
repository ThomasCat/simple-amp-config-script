# Simple AMP config script

A simple, lightweight script that lets you start, stop, configure startup options and install the Apache-MySQL-PHP suite (AMP), instead of Tomcat. 

To use this, open Terminal and type:

```git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/simple-amp-config-script/```

Then extract it and change to its directory:

```unzip simple-amp-config-script.zip```

```cd simple-amp-config-script```

Give it execute permissions:

```chmod +x AMPconfig.sh```

And finally execute it:

```./AMPconfig.sh```

<hr>

[Open-source notices](NOTICE)

<b>License</b>:<br>
<a href="https://www.apache.org/licenses/LICENSE-2.0" rel="nofollow"><img src="https://www.apache.org/img/ASF20thAnniversary.jpg" alt="Apache Image" data-canonical-src="https://www.apache.org/img/ASF20thAnniversary.jpg" width="90"></a><br>[Copyright © Owais Shaikh 2019](LICENSE)
